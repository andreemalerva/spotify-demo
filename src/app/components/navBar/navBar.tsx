"use client";

import Image from "next/image";
import styles from "../../../../public/css/navBar.module.css";
import Link from 'next/link';
import React from "react";

export default function NavBar() {
    const imageHome = React.useRef<HTMLImageElement | null>(null);
    const imageSearch = React.useRef<HTMLImageElement | null>(null);
    const imageLibrary = React.useRef<HTMLImageElement | null>(null);
    var imgHome ="../icons/home.svg";
    var imgSearch = "../icons/Search_S.svg";
    var imgLibrary = "../icons/Library_S.svg";
    var imgCreatePlaylist = "../icons/+Library_S.svg";
    var imgLiked ="../icons/Liked Songs_S.svg";

    const handleClick = (_Event: React.MouseEvent<HTMLAnchorElement, MouseEvent>, path: string) => {  
        if (path === "/") {
            imageHome.current!.src = "../icons/Home_Fill_S.svg";
            imageSearch.current!.src =  "../icons/Search_S.svg";
            imageLibrary.current!.src = "../icons/Library_S.svg";
          }   
         if (path === "/search") {
            imageHome.current!.src ="../icons/home.svg";
            imageSearch.current!.src = "../icons/Search_Fill_S.svg";
            imageLibrary.current!.src = "../icons/Library_S.svg";
         }
         if (path === "/library") {
            imageHome.current!.src ="../icons/home.svg";
            imageLibrary.current!.src = "../icons/Library_Fill_S.svg";
            imageSearch.current!.src =  "../icons/Search_S.svg";
          }
       };

    return (
        <nav className={styles.burguerBox}>
            <Link href="/" onClick={(e) => handleClick(e, "/")} className={styles.burguerItem}>
                <Image className="home" ref={imageHome} src={imageHome.current?.src || imgHome} width={32} height={32} alt="home" />Home
            </Link>
            <Link href="/search" onClick={(e) => handleClick(e, "/search")} className={styles.burguerItem}>
                <Image className="search" ref={imageSearch} src={imageSearch.current?.src || imgSearch} width={32} height={32} alt="search" />Search
            </Link>
            <Link href="/library" onClick={(e) => handleClick(e, "/library")} className={styles.burguerItem}>
                <Image className="library" ref={imageLibrary} src={imageLibrary.current?.src || imgLibrary} width={32} height={32} alt="library" />Your Library
            </Link>
            <Link href="/createPlaylist" onClick={(e) => handleClick(e, "/createPlaylist")} className={styles.burguerItemCreate}>
                <Image className="createPlaylist" src={imgCreatePlaylist} width={32} height={32} alt="library" />Create Playlist
            </Link>
            <Link href="/liked" onClick={(e) => handleClick(e, "/liked")} className={styles.burguerItem}>
                <Image className="liked" src={imgLiked} width={32} height={32} alt="library" />Liked Songs
            </Link>
        </nav>
    )
}