"use client";

import Image from "next/image";
import midCards from "../../../../public/css/midCards.module.css";
import Link from 'next/link';
import React, { useEffect, useRef } from "react";
import { StaticImport } from "next/dist/shared/lib/get-img-props";

export default function MidCards(props: { play: string; case: string | StaticImport; title: string | number | boolean | React.ReactElement<any, string | React.JSXElementConstructor<any>> | Iterable<React.ReactNode> | React.ReactPortal | React.PromiseLikeOfReactNode | null | undefined; reproduction: string | StaticImport; }) {
    const imgPlay = useRef<HTMLImageElement | null>(null);
    const gif = useRef<HTMLImageElement | null>(null);
    
    useEffect(() => {
        if (props.play === "yes") {
            gif.current?.classList.add(midCards.showImgPlay);
            gif.current?.classList.remove(midCards.hiddenImgPlay);
         } 
        
        if (props.play === "none")  {
            gif.current?.classList.remove(midCards.showImgPlay);
            gif.current?.classList.add(midCards.hiddenImgPlay);
        }
      }, []);

    function showImgPlay(e: any) {
        imgPlay.current?.classList.remove(midCards.hiddenImgPlay);
        imgPlay.current?.classList.add(midCards.showImgPlay);
        if (props.play === "yes") {
            gif.current?.classList.remove(midCards.showImgPlay);
            gif.current?.classList.add(midCards.hiddenImgPlay);
        }
    }

    function noneImgPlay(e: any) {
        imgPlay.current?.classList.remove(midCards.showImgPlay);
        imgPlay.current?.classList.add(midCards.hiddenImgPlay);
        if (props.play === "yes") {
            gif.current?.classList.add(midCards.showImgPlay);
            gif.current?.classList.remove(midCards.hiddenImgPlay);
         } 
    }

    return (
        <Link href="/" className={midCards.cardBox} onMouseOver={showImgPlay} onMouseOut={noneImgPlay}>
            <div className={midCards.playlist}>
                <Image className="home" src={props.case} width={82} height={82} alt="chill" />
                {props.title}
            </div>
            <button className={midCards.hover}>
                <Image ref={imgPlay} className={`${midCards.imgPlay} ${midCards.hiddenImgPlay}`} src={props.reproduction} width={62} height={62} alt="chill" />
                <Image ref={gif} className={`${midCards.gifPlay}`} src="/../icons/equalizador.gif" width={18} height={62} alt="play gif" />
            </button>
        </Link>
    )

    
}