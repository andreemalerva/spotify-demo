"use client";

import Image from "next/image";
import styles from "../../../../public/css/header.module.css";
import Link from 'next/link';
import { useState } from 'react'
import React from "react";

export default function HeaderNav() {
    const [display, setDisplay] = useState(false)


    return (
        <div className={styles.navBackground}>
            <div className="buttonChanges">
                <button><Image src="" alt="" /></button>
                <button><Image src="" alt="" /></button>
            </div>
            <div className="profile">
                <nav>
                    <button
                        className="h-10 px-2 rounded-md flex items-center justify-center focus:outline-none"
                        aria-label="Language selector"
                    />
                    <span className="cursor-pointer" onClick={() => setDisplay(!display)}>
                        <svg
                            viewBox="0 0 24 24"
                            width="24"
                            height="24"
                            stroke="currentColor"
                            strokeWidth="1.5"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            fill="none"
                            shapeRendering="geometricPrecision"
                        >
                            <path d="M6 9l6 6 6-6" />
                        </svg>
                    </span>
                </nav>
            </div>

            <div className="absolute top-0 right-0">
                {display && (
                    <div className="fixed right-0 top-10 mt-2 origin-top-right outline-none bg-white z-40 w-full h-full md:absolute md:border md:border-accents-1 md:shadow-lg md:w-56 md:h-auto">
                        <div className="flex flex-row justify-end px-6">
                            <button
                                onClick={() => setDisplay(false)} aria-label="Close panel"
                                className="transition ease-in-out duration-150 hover:text-gray-500 md:hidden">
                            </button>
                        </div>
                        <ul>
                            <li>andree</li>
                        </ul>
                    </div>
                )}
            </div>
        </div>
    )
}